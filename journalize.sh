#!/bin/zsh

# Add newlines in between all adjacent HTML elements so the pattern
# matching will work correctly in awk
sed_script='s!><!>\
<!g'

# Surround each <h2> section with <article>
awk_script=$'
  # Set h2 counter
  BEGIN {
    seen_h2 = 0
    main = 0
  }

  # Start cycling through all lines

  # Don\'t alter any h2\'s in outside of the main block
  /<main>/ { print $0; main = 1; next }
  /<\\/main>/ { print "</article>" $0; main = 0; next }

  /<h2>/ {
    # Skip if not in main block
    if (main == 0) {
      print $0
      next
    }
    # If it\'s the first one, don\'t add ending
    if (seen_h2 == 0) {
      seen_h2 = 1
    } else {
      print "</article>"
    }
    print "<article>" $0
    next
  }
  # If patterns above didn\'t match, do the following
  {
    print $0
  }
'

file=$(cat "$1" | \
  sed -e "$sed_script" | \
  awk "$awk_script")

mv "$1" "${1}-bak"

printf "$file" > "$1"

