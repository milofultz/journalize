# Journalize

This is a little shell script I made so that I could write an [HTML journal](https://journal.miso.town/) in Gemini, convert it to HTML using [gemkill](https://codeberg.org/milofultz/gemkill), and then add the necessary `<article>` tags afterward with this. It uses `awk` and `sed`.

```shell
$ # Convert `journal.html` to valid HTML Journal
$ ./journalize.sh journal.html
```

## What It Expects

An HTML page that contains a `<main>` element. This `<main>` will contain all the HTML journal entries. The HTML journal entries must start with an `<h2>` element. The final journal entry will be closed directly before the closing `</main>` tag.

For example, an input HTML file would look like this, which is what would come out of a normal gemini page rendered with gemkill:

```html
<html>
<head>
  <!-- ... -->
</head>
<body>
  <main>
    <h1>m15o's journal</h1>

    <h2>2022-06-14</h2>
    <p>My second entry.</p>

    <h2>2022-06-11</h2>
    <p>My first entry!</p>
  </main>
</body>
</html>
```

The output post-journalize will look like this (after formatting):

```html
<html>
<head>
  <!-- ... -->
</head>
<body>
  <main>
    <h1>m15o's journal</h1>

    <article>
      <h2>2022-06-14</h2>
      <p>My second entry.</p>
    </article>

    <article>
      <h2>2022-06-11</h2>
      <p>My first entry!</p>
    </article>
  </main>
</body>
</html>
```

